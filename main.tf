provider "aws" {
    region = "ap-south-1"
}

variable vpc_cidr_block {
  type        = string
  default     = ""
  description = "myapp_vpc_IPs"
}

variable subnet_cidr_block {
  type        = string
  default     = ""
  description = "myapp_subnet_IPs"
}

variable avail_zone {
  type        = string
  default     = ""
  description = "availability_zone"
}

variable env_prefix {
  type        = string
  default     = ""
  description = "environment prefix"
}

variable my_ip {
  type        = string
  default     = ""
  description = "current ip"
}

variable instance_type {
  type        = string
  default     = ""
  description = "instance type"
}

variable public_key_location {
  type        = string
  default     = ""
  description = "ssh public key location"
}

variable private_key_location {
  type        = string
  default     = ""
  description = "ssh private key location"
}



resource "aws_vpc" "myapp-vpc" {
    cidr_block = var.vpc_cidr_block
    tags = {
        Name: "${var.env_prefix}-vpc"
    }
}

resource "aws_subnet" "myapp-subnet-1" {
    vpc_id = aws_vpc.myapp-vpc.id
    cidr_block = var.subnet_cidr_block
    availability_zone = var.avail_zone
    tags = {
        Name: "${var.env_prefix}-subnet-1"
    }
}
 
resource "aws_route_table" "myapp-route-table" {
    vpc_id = aws_vpc.myapp-vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.myapp-igw.id
    }
    tags = {
        Name: "${var.env_prefix}-rtb"
    }
}

resource "aws_internet_gateway" "myapp-igw" {
    vpc_id = aws_vpc.myapp-vpc.id
    tags = {
        Name: "${var.env_prefix}-igw"
    }
}

resource "aws_route_table_association" "rtb1-subnet-1" {
    subnet_id = aws_subnet.myapp-subnet-1.id
    route_table_id = aws_route_table.myapp-route-table.id
}

resource "aws_security_group" "myapp-sg" {
    name = "myapp-sg"
    vpc_id = aws_vpc.myapp-vpc.id

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [var.my_ip]
    }

    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        prefix_list_ids = []
    }
    tags = {
        Name: "${var.env_prefix}-sg"
    }

}

data "aws_ami" "latest-amazon-linux-image" {
    most_recent = true
    owners =["amazon"]
    filter {
        name = "name"
        values = ["amzn2-ami-hvm-*-x86_64-gp2"]
    }
    filter {
        name = "virtualization-type"
        values = ["hvm"]
    }

}

output "aws_ami_id" {
    value = data.aws_ami.latest-amazon-linux-image.id
}

resource "aws_key_pair" "ssh-key" {
    key_name = "server-key"
    public_key = file(var.public_key_location)

}

resource "aws_instance" "myapp-server" {
    ami = data.aws_ami.latest-amazon-linux-image.id
    instance_type = var.instance_type

    subnet_id = aws_subnet.myapp-subnet-1.id
    vpc_security_group_ids = [aws_security_group.myapp-sg.id]
    availability_zone = var.avail_zone

    associate_public_ip_address = true
    key_name = aws_key_pair.ssh-key.key_name

    user_data = file("entry-script.sh")

    # connection {
    #     type = "ssh"
    #     host = self.public_ip
    #     user = "ec2-user"
    #     private_key= file(var.private_key_location)
    # }

    # provisioner "file" {
    #     source = "entry-script.sh"
    #     destination = "/home/ec2-user/entry-script.sh"
    # }

    # provisioner "remote-exec" {
    #     # inline = [
    #     #     "export ENV=dev",
    #     #     "mkdir myapp-dir"
    #     # ]
    #     script = file("entry-script.sh")

        
    # }

    # provisioner "local-exec" {
    #     command = "echo ${self.public_ip}"
    # }

    tags = {
        Name: "${var.env_prefix}-server"
    }
}